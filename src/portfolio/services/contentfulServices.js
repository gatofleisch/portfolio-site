const client = require('../config/contentful')
// param to order content on call
// order: 'sys.createdAt'
function fetchWebEntries () {
  return client.getEntries({'content_type':'web'})
  .then((entries) => entries.items)
  .catch(console.error)
}
function fetchVideoEntries () {
  return client.getEntries({'content_type':'video'})
  .then(entries => entries.items )
  .catch(console.error)
}
module.exports = {
  fetchWebEntries,
  fetchVideoEntries
};
