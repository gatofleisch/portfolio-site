import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import {fetchWebEntries, fetchVideoEntries } from './services/contentfulServices';
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      webEntries:[],
      videoEntries:[]
    }
  }

  componentWillMount(){
    fetchWebEntries().then((webEntries)=>{
      fetchVideoEntries().then((videoEntries)=>{ this.setState({videoEntries, webEntries })})
    });

  }
  renderWeb(){
    let {webEntries} = this.state;

    return webEntries.map((entry) => {
      return <li> {entry.fields.name}</li>
    })
  }

  renderVideo(){
    let {videoEntries} = this.state;

    return videoEntries.map((entry) => {
      return <li> {entry.fields.title} </li>
    })
  }
  render() {
    console.log('web', this.state.webEntries)
    console.log('video', this.state.videoEntries)
    return (
      <div className="component-app">
        <h1>WEB PROJECTS</h1>
        <ul>
          {this.renderWeb()}
        </ul>

        <h1>Video PROJECTS</h1>
        <ul>
          {this.renderVideo()}
        </ul>
      </div>
    );
  }
}

export default App;
